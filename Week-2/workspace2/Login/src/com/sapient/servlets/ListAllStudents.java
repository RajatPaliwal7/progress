package com.sapient.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.model.StudentBean;
import com.sapient.model.StudentDAO;

/**
 * Servlet implementation class ListAllStudents
 */
public class ListAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
    StudentDAO sDAO;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListAllStudents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		
		sDAO = new StudentDAO();
		out.print("<meta charset='UTF-8'><html><body><center><h5>");
		List<StudentBean> list = sDAO.getStudentDetails();
		for(StudentBean s : list) {
			out.println(s);
		}
		out.print("</h5></center></body></html>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
