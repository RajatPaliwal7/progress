package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ListPage
 */
public class Listpg extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Listpg() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			int option=Integer.parseInt(request.getParameter("option"));
		    int logid=Integer.parseInt(request.getParameter("logid"));
			int lvl=Integer.parseInt(request.getParameter("lvl"));
			if(option==0)
			{
				int logid1=Integer.parseInt(request.getParameter("slogid"));
				DAOClass.delete(logid1);
			}
			
			List<StudentRecord> list=DAOClass.display();
			String i="<html><body><a href='HomePage?logid="+logid+"&lvl="+lvl+"'>Home</a><br>" + 
					"<a href='LoginPage'>Logout</a><br><center><h1>ListPage</h1><br><table><tr><th>LoginID</th>"
					+ "<th>Roll No</th>"
					+ "<th>FirstName</th>"
					+ "<th>LastName</th>"
					+ "<th>Marks</th>"
					+ "<th>Class</th>"
					+ "<th>Edit</th>"
					+ "<th>Delete</th></tr>";
			for(StudentRecord s: list)
			{
				i=i+"<tr><td>"+s.getLogid()+"</td><td>"+s.getRoll()+"</td><td>"+s.getFname()+"</td><td>"+s.getLname()+"</td><td>"+s.getMarks()+"</td><td>"+s.getYear()+"</td><td><a href='EditPage?logid="+logid+"&lvl="+lvl+"&slogid="+s.getLogid()+"'>Edit</a></td><td><a href='ListPage?logid="+logid+"&lvl="+lvl+"&slogid="+s.getLogid()+"&option=0'>Delete</a></td></tr>";
			}
			String tab=i+"</table></center></body></html>";
			out.print(tab);
		}
		catch(Exception e)
		{}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			int logid=Integer.parseInt(request.getParameter("logid"));
			int lvl=Integer.parseInt(request.getParameter("lvl"));
			int logid1=Integer.parseInt(request.getParameter("slogid"));
			int roll =Integer.parseInt(request.getParameter("roll"));
			String fname=request.getParameter("fname");
			String lname=request.getParameter("lname");
			float marks=Float.parseFloat(request.getParameter("marks"));
			String year=request.getParameter("year");
			DAOClass.update(logid1,roll, fname, lname, marks, year);
			List<StudentRecord> list=DAOClass.display();
			String i="<html><body><a href='HomePage?logid="+logid+"&lvl="+lvl+"'>Home</a><br>" + 
					"<a href='LoginPage'>Logout</a><br><center><h1>ListPage</h1><br><table><tr><th>LoginID</th>"
					+ "<th>Roll No</th>"
					+ "<th>FirstName</th>"
					+ "<th>LastName</th>"
					+ "<th>Marks</th>"
					+ "<th>Class</th>"
					+ "<th>Edit</th>"
					+ "<th>Delete</th></tr>";
			for(StudentRecord s: list)
			{
				i=i+"<tr><td>"+s.getLogid()+"</td><td>"+s.getRoll()+"</td><td>"+s.getFname()+"</td><td>"+s.getLname()+"</td><td>"+s.getMarks()+"</td><td>"+s.getYear()+"</td><td><a href='EditPage?logid="+logid+"&lvl="+lvl+"&slogid="+s.getLogid()+"'>Edit</a></td><td><a href='ListPage?logid="+logid+"&lvl="+lvl+"&slogid="+s.getLogid()+"&option=0'>Delete</a></td></tr>";
			}
			String tab=i+"</table></center></body></html>";
			out.print(tab);
		}
		catch(Exception e)
		{}
	}

}
