package p1;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class Demo1 {
	
	private static URI getBaseUri() 
	{
		
		return UriBuilder.fromUri("http://10.151.61.161:8048/RestDemo").build();
		
		
	}
	
	public static void main(String[] args) {
		ClientConfig config =new ClientConfig();
		Client client= ClientBuilder.newClient(config);
		WebTarget target= client.target(getBaseUri());
		System.out.println(target.path("rest/hello/Iwanttorunthis").request().accept(MediaType.TEXT_PLAIN).get(String.class));
	}
	
	
	

}
