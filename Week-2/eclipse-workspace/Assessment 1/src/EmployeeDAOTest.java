import java.util.ArrayList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
public class EmployeeDAOTest {
	private static EmployeeDAO mockedDAO;
	@BeforeClass
	public static void mocking() {
		mockedDAO = mock(EmployeeDAO.class);
		List<EmployeeBean> lo = new ArrayList<>();
		lo.add(new EmployeeBean("Sudarshan",1,10000f));
		lo.add(new EmployeeBean("Rajat",2,20000f));
		lo.add(new EmployeeBean("Sidhanth",3,30000f));
		lo.add(new EmployeeBean("Ajay",4,40000f));
		lo.add(new EmployeeBean("Chinmay",5,50000f));
		lo.add(new EmployeeBean("Gunjan",6,60000f));
		lo.add(new EmployeeBean("Mohit",7,70000f));
		lo.add(new EmployeeBean("Shubham",8,80000f));
		lo.add(new EmployeeBean("Kanupriya",9,90000f));
		lo.add(new EmployeeBean("Gurpal",10,10000f));
		lo.add(new EmployeeBean("Ketan",11,20000f));
		lo.add(new EmployeeBean("Balbeer",12,30000f));
		when(mockedDAO.readData()).thenReturn(lo);
	}
	@Test
	public void testforReadData() {
		/*List<EmployeeBean> lo = new ArrayList<EmployeeBean>();
		lo.add(new EmployeeBean("Sudarshan",1,10000f));
		lo.add(new EmployeeBean("Rajat",2,20000f));
		lo.add(new EmployeeBean("Sidhanth",3,30000f));
		lo.add(new EmployeeBean("Ajay",4,40000f));
		lo.add(new EmployeeBean("Chinmay",5,50000f));
		lo.add(new EmployeeBean("Gunjan",6,60000f));
		lo.add(new EmployeeBean("Mohit",7,70000f));
		lo.add(new EmployeeBean("Shubham",8,80000f));
		lo.add(new EmployeeBean("Kanupriya",9,90000f));
		lo.add(new EmployeeBean("Gurpal",10,10000f));
		lo.add(new EmployeeBean("Ketan",11,20000f));
		lo.add(new EmployeeBean("Balbeer",12,30000f));
		boolean flag = true;
		EmployeeDAO e = new EmployeeDAO();
		for(int i =0;i<lo.size();i++) {
			if(!lo.get(i).equals(e.readData().get(i)))
				{flag = false;break;}
		}
		*/
		List<EmployeeBean> lo = mockedDAO.readData();
	//	Assert.assertEquals(lo, new EmployeeDAO().readData());
	}
	@Test
	public void testforTotSal() {
		List<EmployeeBean> lo = mockedDAO.readData();
		
/*
		List<EmployeeBean> lo = new ArrayList<EmployeeBean>();
		lo.add(new EmployeeBean("Sudarshan",1,10000f));
		lo.add(new EmployeeBean("Rajat",2,20000f));
		lo.add(new EmployeeBean("Sidhanth",3,30000f));
		lo.add(new EmployeeBean("Ajay",4,40000f));
		lo.add(new EmployeeBean("Chinmay",5,50000f));
		lo.add(new EmployeeBean("Gunjan",6,60000f));
		lo.add(new EmployeeBean("Mohit",7,70000f));
		lo.add(new EmployeeBean("Shubham",8,80000f));
		lo.add(new EmployeeBean("Kanupriya",9,90000f));
		lo.add(new EmployeeBean("Gurpal",10,10000f));
		lo.add(new EmployeeBean("Ketan",11,20000f));
		lo.add(new EmployeeBean("Balbeer",12,30000f));
	*/
		Assert.assertEquals(510000.0f, new EmployeeDAO().getTotSal(lo),1e-15);
	}
	@Test
	public void testforGetCount() {
	/*	List<EmployeeBean> lo = new ArrayList<EmployeeBean>();
		lo.add(new EmployeeBean("Sudarshan",1,10000f));
		lo.add(new EmployeeBean("Rajat",2,20000f));
		lo.add(new EmployeeBean("Sidhanth",3,30000f));
		lo.add(new EmployeeBean("Ajay",4,40000f));
		lo.add(new EmployeeBean("Chinmay",5,50000f));
		lo.add(new EmployeeBean("Gunjan",6,60000f));
		lo.add(new EmployeeBean("Mohit",7,70000f));
		lo.add(new EmployeeBean("Shubham",8,80000f));
		lo.add(new EmployeeBean("Kanupriya",9,90000f));
		lo.add(new EmployeeBean("Gurpal",10,10000f));
		lo.add(new EmployeeBean("Ketan",11,20000f));
		lo.add(new EmployeeBean("Balbeer",12,30000f));
		*/
		List<EmployeeBean> lo = mockedDAO.readData();
		Assert.assertEquals(2, new EmployeeDAO().getCount(20000f,lo));
	}
	@Test
	public void testForGetID() {
		try{EmployeeBean e = new EmployeeBean("Sudarshan",1,10000f);
		boolean flag = e.equals(new EmployeeDAO().getEmployee(1));
		Assert.assertEquals(true,flag);
	}
		catch(UserNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	@Test
	public void testForGetID1() {
		try{ 
			EmployeeBean e = new EmployeeBean("Sudarshan",1,10000f);
		boolean flag = e.equals(new EmployeeDAO().getEmployee(14));
		Assert.fail();
	}
		catch(UserNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
}
