package com.sapient.week1;

import java.util.Scanner;

public class MainMatrix {
	private int[][] matrix1;
	private Scanner sc = new Scanner(System.in);
	public MainMatrix() {
		matrix1 = new int[3][3];
	}
	public MainMatrix(int row, int column) {
		matrix1 = new int[row][column];
	}
	public MainMatrix(int array[][]) {
		matrix1 = array;
	}
	public MainMatrix(MainMatrix ob) {
		matrix1 = ob.matrix1;
	}
	public int[][] getMatrix1() {
	return matrix1;
	}
	public void setMatrix1(int[][] matrix1) {
	this.matrix1 = matrix1;
	}
	public void read() {
		System.out.println("Enter the elements of the matrix");
		for(int i =0;i<this.matrix1.length;i++) {
			for(int j=0;j<this.matrix1[i].length;j++) {
				this.matrix1[i][j] = sc.nextInt();
			}
		}
	}
	public void display() {
		for(int i =0;i<this.matrix1.length;i++) {
			for(int j=0;j<this.matrix1[i].length;j++) {
				System.out.print(this.matrix1[i][j]+" ");
			}
			System.out.println();
		}
	}
	public MainMatrix add(MainMatrix ob) {
		MainMatrix ob1 = new MainMatrix();
		for(int i =0;i<this.matrix1.length;i++) {
			for(int j=0;j<this.matrix1[i].length;j++) {
				ob1.matrix1[i][j] = this.matrix1[i][j]+ob.matrix1[i][j];
			}
		}
		return ob1;
	}
	public MainMatrix sub(MainMatrix ob) {
		MainMatrix ob1 = new MainMatrix();
		for(int i =0;i<this.matrix1.length;i++) {
			for(int j=0;j<this.matrix1[i].length;j++) {
				ob1.matrix1[i][j] = this.matrix1[i][j]-ob.matrix1[i][j];
			}
		}
		return ob1;
	}
	public MainMatrix mul(MainMatrix ob) {
		MainMatrix ob1 = new MainMatrix();
		if(this.matrix1[0].length == ob.matrix1.length) {
		for(int i =0;i<this.matrix1.length;i++) {
			for(int j=0;j<this.matrix1[i].length;j++) {
				ob1.matrix1[i][j] += this.matrix1[i][j]*ob.matrix1[j][i];
			}
		}
		}
		return ob1;
	}
	public static void main(String[] args) {
		MainMatrix ob = new MainMatrix();
		MainMatrix ob1 = new MainMatrix(new int[][]{{1,2,3},{4,5,6},{7,8,9}}); 
		ob.read();
		MainMatrix ob2 = new MainMatrix();
		ob2 = ob1.add(ob);
		ob2.display();
		ob2 = ob1.sub(ob);
		ob2.display();
		ob2= ob1.mul(ob);
		ob2.display();
	}
}
