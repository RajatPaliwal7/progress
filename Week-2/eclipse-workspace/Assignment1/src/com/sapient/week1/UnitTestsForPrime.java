package com.sapient.week1;

import org.junit.Assert;
import org.junit.Test;

public class UnitTestsForPrime {

	@Test
	public void evenNumber() {
		Assert.assertEquals("Not Prime", Prime.primeOrNot(24));
	}
	@Test
	public void oddButPrime() {
		Assert.assertEquals("Prime", Prime.primeOrNot(17));
	}
	@Test
	public void oddButNotPrime() {
		Assert.assertEquals("Not Prime", Prime.primeOrNot(27));
	}
	
}
