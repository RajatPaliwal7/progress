import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CricketDAO {
	public List<PlayerBean> getPlayers() throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("select id,fname,lname,jerseyno from playerinfo");
		ResultSet rs = ps.executeQuery();
		List<PlayerBean> lo = new ArrayList<>();
		PlayerBean player = null;
		while(rs.next()) {
			 player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			 lo.add(player);
		}
		return lo;
	}
	public List<PlayerBean> getAnyPlayer(int id) throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("select id,fname,lname,jerseyno from playerinfo where id =? ");
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		List<PlayerBean> lo = new ArrayList<>();
		PlayerBean player = null;
		while(rs.next()) {
			 player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
			 lo.add(player);
		}
		return lo;
	}
	public int insertPlayer(PlayerBean ob) throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("insert into playerinfo (id,fname,lname,jerseyno) values (?,?,?,?) ");
		ps.setInt(1, ob.getId());ps.setString(2, ob.getFname());ps.setString(3, ob.getLname());ps.setInt(4, ob.getJerseyno());
		int rs = ps.executeUpdate();
		return rs;
	}
	public int updatePlayer(PlayerBean ob) throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("update playerinfo set fname=?,lname=?,jerseyno=? where id =?");
		ps.setString(1, ob.getFname());ps.setString(2, ob.getLname());ps.setInt(3, ob.getJerseyno());ps.setInt(4, ob.getId());
		int rs = ps.executeUpdate();
		return rs;
	}
	public int deletePlayer(int id) throws Exception{
		Connection co = DBConnect.getConnection();
		PreparedStatement ps = co.prepareStatement("delete from playerinfo where id =?");
		ps.setInt(1, id);
		int rs = ps.executeUpdate();
		return rs;
	}
}
