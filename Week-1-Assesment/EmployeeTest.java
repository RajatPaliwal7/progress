package com.sapient.week1.day5;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;



class EmployeeTest {

	@Test
	void testForReadData() throws FileNotFoundException, IOException {
		EmployeeDao employee= new EmployeeDao();
		List<EmployeeBean> list=new ArrayList<EmployeeBean>();
		list.add(new EmployeeBean(1,"Rajat",10000));
		list.add(new EmployeeBean(2,"Ajay",20000));
		list.add(new EmployeeBean(3,"Chinmay",30000));
		Assert.assertArrayEquals(list.toArray(),employee.readData().toArray());
	}
	
	@Test
	void testforGetEmployee() throws FileNotFoundException, IOException
	{
		EmployeeDao employee= new EmployeeDao();
		Assert.assertEquals(new EmployeeBean(1,"Rajat",10000),employee.getEmployee(1));
		Assert.assertEquals(new EmployeeBean(2,"Chinmay",20000),employee.getEmployee(2));
	}
	

}
