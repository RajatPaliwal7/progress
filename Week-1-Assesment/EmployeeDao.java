package com.sapient.week1.day5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class EmployeeDao {
	
	public  List<EmployeeBean> readData() throws IOException
	{
		List <EmployeeBean> list = new ArrayList<EmployeeBean>();
		try {
			BufferedReader bf= new BufferedReader(new FileReader("C:\\Users\\rajpaliw\\Documents\\Edetails.csv"));
			String readline=null;
			while((readline= bf.readLine()) != null) 
			{
				String[] parameters = readline.split(",");
				list.add(new EmployeeBean(Integer.parseInt(parameters[0]),parameters[1],Float.parseFloat(parameters[2])));
			}
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	float getotalSal(List <EmployeeBean> ob) 
	{
		float totalsal=ob.stream()
		.map((employee)->employee.getSalary())
		.reduce(0.0f,Float::sum);
		
		return totalsal;
		
	}
	
	public EmployeeBean getEmployee(int id) throws IOException
	{
		
		List <EmployeeBean> list = readData();
		EmployeeBean ob = new EmployeeBean(id,"Random",0);
		EmployeeBean fob=null;
		
		for(EmployeeBean et: list) {
			if(et.equals(ob)) {
				
				fob= new EmployeeBean(et.getId(),et.getName(),et.getSalary());
			}
		}
		return fob;
	}
	
}
