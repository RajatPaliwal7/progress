package p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo3 {
	public static void main(String[] args) {
		try {
		Arithmetic ob;
		ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		ob = (Arithmetic) context.getBean("arithmetic");
		System.out.println(ob.subtract(2, 3.0));
		}
		
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
}
}
