package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect

public class ArithmeticAspect {
	
	@Before("execution( * *.*(double,double))")
public void check1(JoinPoint jpoint) {
	for(Object x : jpoint.getArgs()) {
		double v =(Double)x;
		if(v<0) {
			throw new IllegalArgumentException("Number cannot be negative");
		}
	}
}

	@AfterReturning(pointcut="execution( * *.*(double,double))",returning="retVal")
	public void check2(Object retVal) {
		double x=(Double)retVal;
		if(x<0)
		{
			throw new IllegalArgumentException("Subtraction cannot be negative");
		}
	}
	
	
	@Around("execution( * *.*(double,double))")
	public double check3(ProceedingJoinPoint pjp) throws Throwable
	{
		double y=0.0;
		for(Object x : pjp.getArgs()) {
			double v =(Double)x;
			if(v<0) {
				throw new IllegalArgumentException("Message1");
			}
			
		//Execution	
	    Object retVal = pjp.proceed();

		
		y=(Double)retVal;
		if(y<0)
		{
			throw new IllegalArgumentException("Message2");
		}
		
		
	}
		return y;
	
	}
	
}
