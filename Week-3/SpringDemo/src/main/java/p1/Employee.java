package p1;

public class Employee {

	private int age;
	private String name;
	
	public Employee(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Employee [age=" + age + ", name=" + name + "]";
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	};
		
}
