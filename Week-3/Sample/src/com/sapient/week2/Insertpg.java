package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InsertPage
 */
public class Insertpg extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insertpg() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			int logid=Integer.parseInt(request.getParameter("logid"));
			int lvl=Integer.parseInt(request.getParameter("lvl"));
			String i="<html><body><a href='HomePage?logid="+logid+"&lvl="+lvl+"'>Home</a><br>"
					+ "<a href='LoginPage'>Logout</a><br><center><h1>InsertPageForm</h1><br><form action='InsertPage?logid="+logid+"&lvl="+lvl+"' method='post'>"
					+ "<br><input type='text' name='logid1' placeholder='LoginID'>"
					+ "<br><input type='text' name='roll' placeholder='Roll No'>"
					+ "<br><input type='text' name='fname' placeholder='FirstName'>"
					+ "<br><input type='text' name='lname' placeholder='LastName'>"
					+ "<br><input type='text' name='marks' placeholder='Marks'>"
					+ "<br><input type='text' name='year' placeholder='Year'>"
					+ "<br><br><input type='submit' value='Insert'></form></center></body></html>";
			out.print(i);
		}
		catch(Exception e)
		{}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		try {
			int logid=Integer.parseInt(request.getParameter("logid"));
			int lvl=Integer.parseInt(request.getParameter("lvl"));
			int logid1=Integer.parseInt(request.getParameter("logid1"));
			int roll =Integer.parseInt(request.getParameter("roll"));
			String fname=request.getParameter("fname");
			String lname=request.getParameter("lname");
			float marks=Float.parseFloat(request.getParameter("marks"));
			String year=request.getParameter("year");
			DAOClass.insert(logid1, roll, fname, lname, marks, year);
			String i="<html><body><a href='HomePage?logid="+logid+"&lvl="+lvl+"'>Home</a><br>"
					+ "<a href='LoginPage'>Logout</a><br><center><h1>InsertPageForm</h1><br><form action='InsertPage?logid="+logid+"&lvl="+lvl+"' method='post'>"
					+ "<br><input type='text' name='logid1' placeholder='LoginID'>"
					+ "<br><input type='text' name='roll' placeholder='Roll No'>"
					+ "<br><input type='text' name='fname' placeholder='FirstName'>"
					+ "<br><input type='text' name='lname' placeholder='LastName'>"
					+ "<br><input type='text' name='marks' placeholder='Marks'>"
					+ "<br><input type='text' name='year' placeholder='Year'>"
					+ "<br><br><input type='submit' value='Insert'></form></center></body></html>";
			out.print(i);
		}
		catch(Exception e)
		{}
	}

}
