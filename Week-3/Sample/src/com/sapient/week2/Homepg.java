package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HomePage
 */
public class Homepg extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Homepg() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		try {
			
			int logid=Integer.parseInt(request.getParameter("logid"));
			String password=request.getParameter("password");
			int lvl=DAOClass.getlevel(logid);
			System.out.println(lvl);
			if(lvl==1)
			{
				String i="<html><center><body><h1>AdminPage</h1><br>"
						+ "<br><a href='InsertPage?logid="+logid+"&lvl="+lvl+"'>Insert</a>"
								+ "<br><a href='ListPage?logid="+logid+"&lvl="+lvl+"&option=2'>List</a>"
										+ "</body></center></html>";
				out.print(i);
			}
			else
			{
				String i="<html><body><center><h1>StudentPage</h1><br><form action='HomePage' method='post'>"
						+ "<br><input type='text' name='roll' placeholder='Roll No'>"
						+ "<br><br><input type='submit' value='GetMarks'></form><br><br><h1></h1></center></body></html>";
				out.print(i);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
