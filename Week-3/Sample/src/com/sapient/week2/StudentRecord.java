package com.sapient.week2;

public class StudentRecord {
	private int logid;
	private int roll;
	private String fname;
	private String lname;
	private float marks;
	private String year;
	public StudentRecord() {
		super();
	}
	public StudentRecord(int logid, int roll, String fname, String lname, float marks, String year) {
		super();
		this.logid = logid;
		this.roll = roll;
		this.fname = fname;
		this.lname = lname;
		this.marks = marks;
		this.year = year;
	}
	
	public int getLogid() {
		return logid;
	}
	public void setLogid(int logid) {
		this.logid = logid;
	}
	public int getRoll() {
		return roll;
	}
	
	public void setRoll(int roll) {
		this.roll = roll;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public float getMarks() {
		return marks;
	}
	public void setMarks(float marks) {
		this.marks = marks;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
}
