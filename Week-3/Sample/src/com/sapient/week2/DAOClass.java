package com.sapient.week2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DAOClass {
	public static Connection con;
	
	public static int getlevel(int logid) 
	{
		int lvl=0;
		try {
			con=DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("select lvl from LoginDetails where logid=?");
			ps.setInt(1, logid);
			ResultSet rs= ps.executeQuery();
			rs.next();
			lvl=rs.getInt(1);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lvl;
	}
	public static int getmarks(int roll) 
	{
		int marks=0;
		try {
			con=DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("select marks from StudentRecord where roll=?");
			ps.setInt(1, roll);
			ResultSet rs= ps.executeQuery();
			rs.next();
			marks=rs.getInt(2);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return marks;
	}
	public static void insert(int logid,int roll,String fname, String lname, float marks,String year) 
	{
		
		try {
			con=DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("insert into StudentRecord values(?,?,?,?,?,?)");
			ps.setInt(1, logid);
			ps.setInt(2, roll);
			ps.setString(3, fname);
			ps.setString(4, lname);
			ps.setFloat(5, marks);
			ps.setString(6, year);
			ps.execute();
			PreparedStatement ps2 = con.prepareStatement("insert into LoginDetails values(?,'default',?)");
			ps2.setInt(1, logid);
			ps2.setInt(2, 0);
			ps2.execute();
			con.commit();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static List<StudentRecord> display()
	{
		List<StudentRecord> list = new ArrayList<StudentRecord>();
		try {
			con=DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from StudentRecord");
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				list.add(new StudentRecord(rs.getInt(1),rs.getInt(2),rs.getString(3),rs.getString(4),rs.getFloat(5),rs.getString(6)));
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	public static StudentRecord getStudent(int logid) 
	{
		StudentRecord stud= new StudentRecord();
		try {
			con=DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from StudentRecord where logid=?");
			ps.setInt(1, logid);
			ResultSet rs=ps.executeQuery();
			rs.next();
			stud.setLogid(rs.getInt(1));
			stud.setRoll(rs.getInt(2));
			stud.setFname(rs.getString(3));
			stud.setLname(rs.getString(4));
			stud.setMarks(rs.getFloat(5));
			stud.setYear(rs.getString(6));
		    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stud;
	}
	public static void update(int logid,int roll,String fname, String lname, float marks,String year) 
	{
		
		try {
			con=DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("update StudentRecord set roll=?,fname=?,lname=?,marks=?,year=? where logid=?");
			ps.setInt(6, logid);
			ps.setInt(1, roll);
			ps.setString(2, fname);
			ps.setString(3, lname);
			ps.setFloat(4, marks);
			ps.setString(5, year);
			ps.execute();
			con.commit();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static void delete(int logid) 
	{
		
		try {
			con=DBConnect.getConnection();
			PreparedStatement ps = con.prepareStatement("delete from StudentRecord where logid=?");
			ps.setInt(1, logid);
			ps.execute();
			PreparedStatement ps2 = con.prepareStatement("delete from LoginDetails where logid=?");
			ps2.setInt(1, logid);
			ps2.execute();
			con.commit();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
