package com.sapient.week2;

public class LoginDetails {
	private int logid;
	private String password;
	private int lvl;
	public int getLogid() {
		return logid;
	}
	public void setLogid(int logid) {
		this.logid = logid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getLvl() {
		return lvl;
	}
	public void setLvl(int lvl) {
		this.lvl = lvl;
	}
	@Override
	public String toString() {
		return "LoginDetails [logid=" + logid + ", password=" + password + ", lvl=" + lvl + "]";
	}
}
