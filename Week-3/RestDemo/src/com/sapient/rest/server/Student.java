package com.sapient.rest.server;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/hello")
public class Student{
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getString1() {
		return " ";
	}
	
	@GET
	@Path("/Iwanttorunthis")
	public String getString2() {
		 return "I want this method to execute";
		 
	 }	
}



