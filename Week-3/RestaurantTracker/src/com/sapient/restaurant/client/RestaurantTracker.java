package com.sapient.restaurant.client;

import java.net.URI;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sapient.restaurant.RestaurantDetails;

import org.glassfish.jersey.client.ClientConfig;

public class RestaurantTracker {

	private static URI getLocation(String city)
	{
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/locations?query="+city).build();
	}
	private static URI getLocationDetails(String entity_type,int entity_id)
	{
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/location_details?entity_id="+entity_id+"&entity_type="+entity_type).build();
	}
	private static URI getRestaurant(int res_id)
	{
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/restaurant?res_id="+res_id).build();
	}
	public static String findRestaurants(String city) throws JSONException 
	{
		Map<String,RestaurantDetails> map = new HashMap<String,RestaurantDetails>();
		ClientConfig config = new ClientConfig();
		Client client= ClientBuilder.newClient(config);
		WebTarget target= client.target(getLocation(city));
		String jsonString=target.request().header("user-key","b22c755aa188f4eeff7906f4e567a460").accept(MediaType.APPLICATION_JSON).get(String.class);
		JSONObject obj = new JSONObject(jsonString);
		JSONArray array=(JSONArray)obj.get("location_suggestions");
		JSONObject objn=(JSONObject)array.get(0);
		String entity_type=(String)objn.get("entity_type");
		int entity_id=(int)objn.get("entity_id");
		WebTarget target2= client.target(getLocationDetails(entity_type,entity_id));
		String jsonString2=target2.request().header("user-key","b22c755aa188f4eeff7906f4e567a460").accept(MediaType.APPLICATION_JSON).get(String.class);
	
		return jsonString2;
	}
}