package com.sapient.restaurant;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class RestaurantDao {

	String apikey ="9af9a317689953505cda2ccf83374770";
	

	private static URI getBaseUri(String lat, String lon) 
	{
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/geocode?lat="+lat+"&lon="+lon+"").build();
	}
	
	public void GetRestaurantByLocation() {
		ClientConfig config =new ClientConfig();
		Client client= ClientBuilder.newClient(config);
		WebTarget target= client.target(getBaseUri("40.74","-74.00"));
		System.out.println(target.request().header("user-key","9af9a317689953505cda2ccf83374770").accept(MediaType.TEXT_PLAIN).get(String.class));
	}
	
}
