package com.sapient.week1;
import java.util.Scanner;

//Class for Checking a prime number
public class IsPrime {  
  
   public static void main(String[] args) 
   {  
	   //Taking a number input
       Scanner s = new Scanner(System.in);  
       System.out.print("Enter a number : ");  
       int n = s.nextInt();  
       
       //Passing the number to function checking for prime number
       if (isPrime(n)) 
       {  
    	   System.out.println(n + " is a prime number");  
       } 
       else 
       {  
           System.out.println(n + " is not a prime number");  
       }  
   }  
  
   public static boolean isPrime(int n) 
   {  
	   //When the number is not a natural number
       if (n <= 1) {return false;}  
       
       //Optimizing the for loop to squareroot for prime number
       for (int i = 2; i <= Math.sqrt(n); i++) 
       {  
           if (n % i == 0) //When the remiander is zero means the number cannot be prime
        	   //Hence returning false	   
           {  
               return false;  
           }  
       }  
       return true;  
   }  
}  