package com.sapient.week1;
import java.util.Arrays;
import java.util.Scanner;

public class IntegerArray {

	private int size;
	private int arr[];
	Scanner sc = new Scanner(System.in);
	
	//Set default size
	public IntegerArray()
	{
		size =10;
		arr = new int[this.size];
	}
	
	//Set size as specified by the user
	public IntegerArray(int x) 
	{
		size=x;
		arr = new int[this.size];
	}
	
	//Copy Constructor
	IntegerArray(IntegerArray x)
	{
		size=x.size;
		arr = x.arr;
	}
	
	//Get and Set functions for Size
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
		
	}
	
	//Method to get values in the array
	public void GetArray()
	{
		for (int i=0;i<size;i++)
		{
			arr[i]=sc.nextInt();
		}	
	}
	
	//Method to display values in the array
	public void DisplayArray()
	{
		for (int i=0;i<size;i++)
		{
			System.out.println(arr[i]);
		}	
		
	}
	
	//Searching in an array
	public void ArraySearch(int k) 
	{
		int loc;
		loc=Arrays.binarySearch(arr, k);
		if(loc>=0)
		{
			System.out.println("Found at"+loc);
		}
		
		else
		{
			System.out.println("Not Found");
		}
		
	}
	
	//Find Average of the array
	public void ArrayAverage()
	{
		int sum=0,avg;
		for (int i=0;i<size;i++)
		{
			sum+=arr[i];
		}	
		avg=sum/size;
		System.out.println(avg);
	}
	
	//Sort a given array
	public void ArraySort()
	{
		Arrays.sort(arr);
		DisplayArray();
	}
	
	
	//Adoption of an array
	public void AdoptArray(int arr[])
	{
		
		this.arr=arr;
		System.out.println("Array Adopted");
		DisplayArray();
	}
	
	

}
