package com.sapient.week1.day3;
import java.util.ArrayList;

public class DuplicateArray{
public static void main(String[] args) {
    DuplicateArray obj = new DuplicateArray();
	//Declaration for Arraylist using collections
	ArrayList<String> list = new ArrayList<String>();
    
	
	list.add("one");
    list.add("one");
    list.add("two");
    list.add("three");
    list.add("three");

    System.out.println("Prior to removal: " + list);
    ArrayList<String> list2 = obj.removeDuplicates(list);
    System.out.println("After removal: " + list2);

}

public static <E> ArrayList<E> removeDuplicates(ArrayList<E> list) {

	ArrayList<E> list2 = new ArrayList<E>();

    for (E elem : list) 
        if (!list2.contains(elem))
            list2.add(elem);

    return list2;
 }

}











