package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrainingController {

@Autowired	
TrainingDAO train;

@PostMapping("/addarow")
public String StoreTCDetails(@RequestBody TrainingBean ob) {
	train.save(ob);
	return "Saved to DB";
}

@RequestMapping("/courses")
public List<TrainingBean> Getallcourses()
	{
		return (List<TrainingBean>)train.findAll();
	}		


@RequestMapping("/courses/{id}")
public Optional<TrainingBean> getcoursebyid(@PathVariable int id)
	{
		return (Optional<TrainingBean>)train.findById(id);
	}		

@RequestMapping("/courses/delete/{id}")
public String deletefunction(@PathVariable int id) {
	
	train.deleteById(id);
	return "Deleted for the required id";
}


}
