package com.example.demo;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class TestCase {

	@Autowired
	RestTemplate rest;
	static final String URL = "http://localhost:8099/";
	@Test
	public void getTestforAll() {
	rest = new RestTemplate();
	String output = rest.getForObject(URL+"courses", String.class);
	//System.out.println(output);
	String getallresult="[{\"id\":1,\"courseTitle\":\"CoreJava\",\"courseEndDate\":\"28/04/2020\",\"fees\":15000.0,\"courseStartdate\":\"15/10/2019\"},{\"id\":3,\"courseTitle\":\"CoreJava\",\"courseEndDate\":\"28/02/2020\",\"fees\":8000.0,\"courseStartdate\":\"15/08/2019\"},{\"id\":4,\"courseTitle\":\"Trees\",\"courseEndDate\":\"28/11/2019\",\"fees\":18000.0,\"courseStartdate\":\"25/06/2019\"},{\"id\":5,\"courseTitle\":\"Graphs\",\"courseEndDate\":\"28/04/2019\",\"fees\":12000.0,\"courseStartdate\":\"12/02/2019\"}]";
	Assert.assertEquals(getallresult,output);
	}
	
	@Test
	public void getTestforId() {
	 rest = new RestTemplate();
	 String output = rest.getForObject(URL+"courses/"+"1", String.class);
	 String getidresult="{\"id\":1,\"courseTitle\":\"CoreJava\",\"courseEndDate\":\"28/04/2020\",\"fees\":15000.0,\"courseStartdate\":\"15/10/2019\"}";
	 Assert.assertEquals(getidresult,output);
	}

}

