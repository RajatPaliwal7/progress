package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="courses")
public class TrainingBean {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(name = "title")
	private String CourseTitle;
	@Column(name = "startdate")
	private String CourseStartdate;
	@Column(name = "enddate")
	private String CourseEndDate;
	@Column(name = "fees")
	private float Fees;
	


	public TrainingBean() {
		super();
	}
	public TrainingBean( String courseTitle, String courseStartdate, String courseEndDate, float fees) {
		super();
		CourseTitle = courseTitle;
		CourseStartdate = courseStartdate;
		CourseEndDate = courseEndDate;
		Fees = fees;
	}
	public String getCourseTitle() {
		return CourseTitle;
	}
	public void setCourseTitle(String courseTitle) {
		CourseTitle = courseTitle;
	}
	public String getCourseStartdate() {
		return CourseStartdate;
	}
	public void setCourseStartdate(String courseStartdate) {
		CourseStartdate = courseStartdate;
	}
	public String getCourseEndDate() {
		return CourseEndDate;
	}
	public void setCourseEndDate(String courseEndDate) {
		CourseEndDate = courseEndDate;
	}
	public float getFees() {
		return Fees;
	}
	public void setFees(float fees) {
		Fees = fees;
	}
	@Override
	public String toString() {
		return "TrainingBean [id=" + id + ", CourseTitle=" + CourseTitle + ", CourseStartdate=" + CourseStartdate
				+ ", CourseEndDate=" + CourseEndDate + ", Fees=" + Fees + "]";
	}
	
	
	
	
}
